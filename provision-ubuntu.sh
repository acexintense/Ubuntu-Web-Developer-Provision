#!/bin/bash
sudo apt-get install lamp-server^
cd ~/Downloads
wget https://download.sublimetext.com/sublime-text_build-3083_amd64.deb
sudo apt-get install gdebi
sudo gdebi sublime-text_build-3083_amd64.deb
sudo apt-get install virtualbox
sudo apt-get install gimp
sudo apt-get install inkscape
sudo apt-get install default-jre
sudo apt-get install default-jdk
gem update --system
gem install compass
sudo apt-get install git
sudo apt-get install vagrant
sudo add-apt-repository ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get install libreoffice
sudo apt-get install vim
sudo apt-get install vim-scripts
sudo reboot